const dotenv      = require('dotenv').config();
var LocalStrategy = require('passport-local');
var User          = require('./models/user.js');
var mongoose      = require("mongoose");
var connectflash  = require("connect-flash");
var jwt           = require("jwt-simple");
const mailgun     = require("mailgun-js");
const api_key     = process.env.API_KEY;
var DOMAIN        = process.env.API_URL;
var SendDomain    = process.env.MAILGUN_POSTMASTER; 
const mg          = mailgun({apiKey: api_key , domain: DOMAIN});
var bcrypt        = require("bcrypt-nodejs");
const crypto      = require('crypto')

module.exports = function (app, passport) {


    app.get('/', function(req, res) {
        res.render('index.ejs'); //load the index.ejs file and renders it
    });

    app.get('/login', function (req, res) {
        //will render our login page with some flash messages if needed
        res.render('login.ejs', { message: req.flash('loginMessage') });
    });

    app.post('/login', function(req, res, next) {
      //console.log({"req.user.local.password": req.body});
      var passwordHashed = crypto.createHash('md5').update(req.body.password).digest("hex")
      req.body.password = passwordHashed;

      passport.authenticate('local-login', function(err, user, info) {
        if (err) { return next(err); }
        if (!user) {  res.status(401); }
        req.logIn(user, function(err) {
          if (err) {
            return res.redirect('/login');
          }
          if (user) {
            return res.redirect('/profile');
          }
        });
      })(req, res, next);
    });


    app.post('/godotlogin', function(req, res, next) {
      passport.authenticate('local-login', function(err, user, info) {
        if (err) { return next(err); }
        if (!user) {  res.status(401); }
        req.logIn(user, function(err) {
          if (err) { return next(err); }
          if(user){
              var godotUserEmail = req.user.local.email;
              var godotUserId    = req.user._id;
              var godotUserPW    = req.user.local.password;
              var godotUserJsalt = req.user.local.userJwtSalt;
              var payload = {
                  id: godotUserId,
                  email: godotUserEmail
              }
              var secret = godotUserPW + godotUserJsalt;
              var token = jwt.encode(payload, secret);

              res.json({ id: godotUserId, token:  token });
              }
        });
      })(req, res, next);
    });


    //show the signup form
    app.get('/signup', function(req, res ) {
        //render the page and pass in the flash stuff if needed
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });


    //process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/profile', //redirect our user to the secure profile page
        failureRedirect : '/signup', // redirect back to signup page if there is an error
        failureFlash : true

    }));

    // This part will be protected and you have to be logged in to visit this page
    //We will use route middleware to perform this ( the isLoggedIn function)
    app.get('/profile', isLoggedIn, function(req, res) {
        res.render('profile.ejs', {
            user: req.user // get the user out of the session and pass him to the template
        });
    });

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/forgotpassword', function(req, res) {
        res.render("forgotpassword.ejs");

    });

    app.post('/passwordreset', function(req, res) {             //route from the passwordreset form in forgotpassword.ejs
        if(req.body.email !== undefined) {
            const emailAdress = req.body.email;

            console.log(emailAdress);  // email adress from userinput


            User.findOne({ 'local.email' :req.body.email }, function(err, userexist){
                if(!userexist) {
                    console.log('error user does not exist')
                    req.flash('no user found ', 'try again')
                    return res.redirect('/forgotpassword')
                }

                    if(userexist) {
                        var emailAddress = req.body.email;
                        console.log('user exist')

                        //creating payload for the token
                        var payload = {
                            id: userexist._id,
                            email: emailAddress
                        };
                        console.log(payload)

                        //using pw hash from db for the token creation
                        var secret = userexist.local.password + userexist.local.userJwtSalt;
                        //generating token with the payload and the pw hash from db
                        var token = jwt.encode(payload, secret);
                        console.log(token)
                        //email body
                        const data = {
                            from: 'Critically-Entangled password reset'+SendDomain,
                            to: userexist.local.email,
                            subject: "Hello",
                            text: "To reset your pw plz klick this link : " + " http://localhost:3000/resetpassword/" + payload.id + '/' + token
                        };
                    //function to send the email with mailgun
                    mg.messages().send(data, function (error, body) {
                        if(error){
                            console.log(error)
                        }else {
                        console.log(body);

                    }})
                    res.redirect('/emailsent')
                }
            })}
        });
    // find user over the id from the get  and creates a 1 time form for the pw reset
    app.get('/resetpassword/:id/:token', function(req, res) {

        User.findOne({_id: req.params.id}, function (err, userexist){

            if(!userexist){

                console.log('error, cant find id in db !')
            }

            if(userexist) {
                var secret = userexist.local.password + userexist.local.userJwtSalt;
                var payload = jwt.decode(req.params.token, secret)

                res.send('<form action="/resetpassword" method="POST">' +
                    '<input type="hidden" name="id" value="' + payload.id + '" />' +
                    '<input type="hidden" name="token" value="' + req.params.token + '" />' +
                    '<input type="password" name="password" value="" placeholder="Enter your new password..." />' +
                    '<input type="submit" value="Reset Password" />' +
                '</form>');
            }
        }
    )});


    app.post('/resetpassword', function(req, res){

        User.findOne({_id: req.body.id}, function(err, userexist){
            if(!userexist){
                console.log('fail')
            }
            if(userexist) {
                var newUserPW = userexist
                var passwordHashed = crypto.createHash('md5').update(req.body.password).digest("hex")

                newUserPW.local.password = bcrypt.hashSync(passwordHashed, bcrypt.genSaltSync(8))

                console.log('-----------> new user pw saved: <---------------' )

                newUserPW.save(function(err) {
                    if (err)
                        throw err;
                    return (null, newUserPW);
                })

                res.redirect('/login')
                }
        })
    })


    app.get('/tokenCheck/:id/:token', function(req, res) {


        User.findOne({_id: req.params.id}, function (err, userexist){

            if(!userexist){

                res.json({id: null, error: 1})
            }

            if(userexist) {
                var secret = userexist.local.password + userexist.local.userJwtSalt;
                var payload = jwt.decode(req.params.token, secret)

                console.log('sending back payload id: ' + payload.id)

                res.json({id: payload.id, error: null});
            }
        })
    })







    app.get('/emailsent', function(req, res) {
        res.render("emailsent.ejs");
    });



};

//route middleware to make sure the user is logged in
function isLoggedIn(req, res, next) {

    //if the user is authenticated in the session , the func will call next to carry on
    if(req.isAuthenticated())
        return next();

    res.redirect('/');

}

//});
