//createt this code with help from a online tutorial on scotch.io 
//(https://scotch.io/tutorials/easy-node-authentication-setup-and-local#undefined)
var dotenv          = require("dotenv").config();
var express         = require("express");
var app             = express();
var mongoose        = require('mongoose');
var passport        = require('passport');
var flash           = require("connect-flash");
var morgan          = require("morgan");
var cookieParser    = require("cookie-parser");
var session         = require("express-session");
var bodyParser      = require("body-parser");
const connectDb     = require("./connection");
const User          = require("./models/user")
const jwt           = require("jwt-simple")
const mailgun       = require("mailgun-js");

require('./config/passport')(passport); //pulls in passport for configuration 
app.use('/public', express.static('public'));
app.use(morgan('dev'));  // this will log every request to the console 
app.use(cookieParser()); // read cookies (is needed for auth)
app.use(bodyParser.urlencoded({ extended: false }));

app.set('view engine', 'ejs'); //set up ejs for templating


//required settings for passport 
app.use(session({ secret: process.env.SESSION_SECRET, })); //session secret 
app.use(passport.initialize());
app.use(passport.session()); //persistent  login seesions
app.use(flash()); //connectflash npm for storring msg s 
app.use(express.static("public"));

require('./routes')(app, passport); //loading our routes and passport

var PORT  = process.env.PORT
app.listen(PORT);
console.log('auth server runs on port: -----------------------------------' + PORT);
connectDb().then(() => {
  console.log('mongo connected')
})




