
var LocalStrategy = require('passport-local');
var User          = require('../models/user.js');
const crypto      = require('crypto')

//with module.exports the function will be exposed to our app


module.exports = function(passport) {


    //this func is used to serialize user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);

    });



    //this function is used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done (err, user);

        });
    });


    ////////////////////////
    //Local Signup /////////
    ////////////////////////
    //We are using named strategies since we have 2 strategies, 1 for login and 1 for signup
    //by default, if there was no name we would name it just 'local'

    passport.use('local-signup', new LocalStrategy({
        //by default the localstrategy uses username and email , we will us email and password
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows to pass back  the entire request to the callback
    },
    function(req, email, password, done) {

        //asynchronus
        //User.findOne wll not fire unless data is sent beack
        process.nextTick(function() {


        //find a user with the same email as the email we get from the form
        //checking if the user whos trying to login allready exist
            User.findOne({ 'local.email' : email }, function(err, user) {
                //if error , return error
                if (err)
                    return done(err);


                //check if theres allready an user with that email
                if (user) {
                    return done(null, false, req.flash('signupMessage', 'That email is allready taken.'))
                }else {


                    //if the email is free and no user allready owns it
                    // create the user

                    var newUser = new User();


                    //set the user's local credentials
                    newUser.local.userJwtSalt = Date.now().toString();
                    newUser.local.email = email;
                    var passwordHashed = crypto.createHash('md5').update(password).digest("hex")
                    newUser.local.password = newUser.generateHash(passwordHashed);

                    console.log(newUser)
                    // Save the User to DB

                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });

        });

    }));









// =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) { // callback with email and password from our form

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'local.email' :  email }, function(err, user) {
            // if there are any errors, return the error before anything else
            if (err)
                return done(err);

            // if no user is found, return the message
            if (!user)
                return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash

            var passwordHashed = password; //crypto.createHash('md5').update(password).digest("hex")
            console.log({passwordHashed: passwordHashed})
            // if the user is found but the password is wrong
            if (!user.validPassword(passwordHashed))
                return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

            // all is well, return successful user
            return done(null, user);
        });

    }));



};
